from django.test import TestCase
from mock import Mock, patch

from related.views import RedirectOnExistingMixin, CreateWithRelatedMixin


class RedirectOnExistingMixinTestCase(TestCase):
    def setUp(self):
        self.request = Mock()
        self.request.REQUEST = Mock()
        self.request.REQUEST.get = Mock()
        self.view = RedirectOnExistingMixin()
        self.view.request = self.request

    def test_get_existing_redirect_url(self):
        self.view.existing_redirect_url = '/foo'
        self.assertEqual(self.view.get_existing_redirect_url(Mock()), '/foo')

    def test_get_existing_kwargs_pk(self):
        kwargs = self.view.get_existing_kwargs()
        self.request.REQUEST.get.assert_called_once_with('pk')
        self.assertEqual(kwargs['pk'], self.request.REQUEST.get.return_value)

    def test_get_existing_kwargs_slug(self):
        def get_mock(key):
            if key == 'slug':
                return 'foo'

        self.request.REQUEST.get = get_mock
        kwargs = self.view.get_existing_kwargs()
        self.assertEqual(kwargs['slug'], 'foo')
        self.assertFalse(kwargs.has_key('pk'))

    def test_get_existing_from_request(self):
        self.view.model = Mock()
        self.view.model.objects = Mock()
        self.view.model.objects.get = Mock()
        self.view.model.objects.get
        existing = self.view.get_existing_from_request()
        self.assertEqual(existing, self.view.model.objects.get.return_value)

    def test_get_existing_form_kwargs(self):
        self.request.method = 'GET'
        kwargs = self.view.get_existing_form_kwargs()
        self.assertEqual(kwargs, dict(initial=dict()))

        self.request.method = 'POST'
        self.request.POST = 'foo'
        self.assertEqual(
            self.view.get_existing_form_kwargs(),
            dict(data='foo', initial=dict())
        )

        self.request.method = 'PUT'
        self.request.POST = 'bar'
        self.assertEqual(
            self.view.get_existing_form_kwargs(),
            dict(data='bar', initial=dict())
        )

    def test_existing_form(self):
        self.request.method = 'POST'
        self.request.POST = dict(foo='bar')
        self.view.existing_form_class = Mock()
        form = self.view.get_existing_form()
        self.view.existing_form_class.assert_called_once_with(
            initial=dict(), data=dict(foo='bar')
        )

    def test_existing_form_with_override(self):
        self.request.method = 'POST'
        self.request.POST = dict(foo='bar')
        self.view.existing_form_class = Mock()
        form = self.view.get_existing_form(data=dict(fam='dam'))
        self.view.existing_form_class.assert_called_once_with(
            initial=dict(), data=dict(fam='dam')
        )

    def test_get_existing_from_form(self):
        mock_form_instance = Mock()
        mock_form_instance.cleaned_data = dict(foo=123)
        mock_form_instance.is_valid = Mock()
        mock_form_instance.is_valid.return_value = True

        self.view.existing_form_field = 'foo'
        self.view.existing_form_class = Mock()
        self.view.existing_form_class.return_value = mock_form_instance

        existing = self.view.get_existing_from_form()
        self.assertEqual(existing, 123)

    def test_get_existing(self):
        self.view.get_existing_from_form = Mock()
        self.view.get_existing_from_request = Mock()

        self.view.get_existing()
        self.view.get_existing_from_form.assert_not_called()
        self.view.get_existing_from_request.assert_called()

        self.view.existing_form_class = True

        self.view.get_existing()
        self.view.get_existing_from_form.assert_called()
        self.view.get_existing_from_request.assert_not_called()

    def test_get_context(self):
        class Super(object):
            def get_context_data(self):
                return dict(foo='bar')

        class View(RedirectOnExistingMixin, Super):
            pass

        view = View()

        view.request = Mock()
        view.request.get = dict(fam='dam')
        view.get_existing_form_class = Mock()
        view.get_existing_form = Mock()

        ctx = view.get_context_data()

        self.assertTrue(ctx.has_key('existing_form'))

    def test_get_context_name(self):
        class Super(object):
            def get_context_data(self):
                return dict(foo='bar')

        class View(RedirectOnExistingMixin, Super):
            pass

        view = View()

        view.request = Mock()
        view.request.get = dict(fam='dam')
        view.get_existing_form_class = Mock()
        view.get_existing_form = Mock()
        view.existing_form_name = 'foo_form'

        ctx = view.get_context_data()

        self.assertTrue(ctx.has_key('existing_form'))
        self.assertTrue(ctx.has_key('foo_form'))

    def test_setting_initial_when_GET_is_empty(self):
        class Super(object):
            def get_context_data(self):
                return dict(foo='bar')

        class View(RedirectOnExistingMixin, Super):
            pass

        view = View()
        view.request = Mock()
        view.request.method = 'GET'
        view.request.GET = dict()
        view.get_existing_form_class = Mock()
        view.get_existing_form = Mock()

        ctx = view.get_context_data()

        self.assertEqual(ctx['existing_form'],
                         view.get_existing_form.return_value)
        view.get_existing_form.assert_called_once_with()

    def test_setting_initial_when_GET_is_not_empty(self):
        class Super(object):
            def get_context_data(self):
                return dict(foo='bar')

        class View(RedirectOnExistingMixin, Super):
            pass

        view = View()
        view.request = Mock()
        view.request.method = 'GET'
        view.request.GET = dict(fam='dam')
        view.get_existing_form_class = Mock()
        view.get_existing_form = Mock()

        ctx = view.get_context_data()

        view.get_existing_form.assert_called_once_with(data=dict(fam='dam'))

    def test_without_form(self):
        class Super(object):
            def get_context_data(self):
                return dict(foo='bar')

        class View(RedirectOnExistingMixin, Super):
            pass

        view = View()

        view.request = Mock()
        view.request.get = dict(fam='dam')

        ctx = view.get_context_data()
        self.assertFalse(ctx.has_key('existing_form'))

    def test_post(self):
        class Super(object):
            def post(self, request):
                return 'foo'

        class View(RedirectOnExistingMixin, Super):
            pass

        view = View()
        view.get_existing = Mock()
        view.get_existing.return_value = None

        response = view.post(Mock())

        self.assertEqual(response, 'foo')

    def test_post_with_existing(self):
        from django.http import HttpResponseRedirect

        class Super(object):
            post = Mock()

        class View(RedirectOnExistingMixin, Super):
            pass

        view = View()
        view.get_existing = Mock()
        view.get_existing.return_value = True
        view.get_existing_redirect_url = lambda x: '/foo'

        response = view.post(Mock())
        self.assertIsInstance(response, HttpResponseRedirect)


class CreateWithRelatedMixinTestCase(TestCase):
    def setUp(self):
        self.request = Mock()
        self.request.GET = Mock()
        self.request.POST = Mock()
        self.request.GET.get = Mock()
        self.request.POST.get = Mock()

        self.view = CreateWithRelatedMixin()
        self.view.request = self.request

    def test_get_related_field(self):
        self.view.related_model = Mock()
        self.view.related_model.__name__ = 'Foo'

        rf = self.view.get_related_field()
        self.assertEqual(rf, 'foo')

        self.view.related_field = 'bar'
        rf = self.view.get_related_field()
        self.assertEqual(rf, 'bar')

    def test_get_object_kwargs_with_pk(self):
        self.view.kwargs = dict(pk='foo')
        kwargs = self.view.get_object_kwargs()
        self.assertEqual(kwargs, dict(pk='foo'))

    def test_get_object_kwargs_with_slug(self):
        self.view.kwargs = dict(slug='bar')
        kwargs = self.view.get_object_kwargs()
        self.assertEqual(kwargs, dict(slug='bar'))

    def test_get_related_obj(self):
        self.view.related_model = Mock()
        self.view.related_model.objects = Mock()
        self.view.related_model.objects.get = Mock()
        self.view.get_object_kwargs = Mock()
        self.view.get_object_kwargs.return_value = dict(pk='foo')

        obj = self.view.get_related_object()
        self.assertEqual(obj, self.view.related_model.objects.get.return_value)

    def test_related_object_not_found(self):
        from django.http import Http404, HttpResponseRedirect

        self.view.request.method = 'GET'

        self.view.related_404_redirect_url = '/foo'
        response = self.view.related_object_not_found()
        self.assertIsInstance(response, HttpResponseRedirect)

        self.view.related_404_redirect_url = None

        with self.assertRaises(Http404):
            self.view.related_object_not_found()

    def test_related_object_gone(self):
        self.view.request.method = 'POST'
        self.view.related_object_gone = Mock()
        response = self.view.related_object_not_found()
        self.view.related_object_gone.assert_called_once()
        self.assertEqual(response, self.view.related_object_gone.return_value)

    def test_context(self):
        class Super(object):
            def dispatch(self, request, *args, **kwargs):
                return

            def get_context_data(self):
                return dict()

        class View(CreateWithRelatedMixin, Super):
            pass

        view = View()
        view.get_related_object = Mock()
        view.dispatch(Mock())  # Fake dispatch call with a mock object

        ctx = view.get_context_data()
        view.get_related_object.assert_called_once()
        self.assertEqual(ctx['related_object'],
                         view.get_related_object.return_value)

    def test_related_object_name(self):
        class Super(object):
            def dispatch(self, request, *args, **kwargs):
                return

            def get_context_data(self):
                return dict()

        class View(CreateWithRelatedMixin, Super):
            pass

        view = View()
        view.get_related_object = Mock()
        view.dispatch(Mock())  # Fake dispatch call with mock object
        view.related_object_name = 'foo_object'

        ctx = view.get_context_data()

        self.assertEqual(ctx['related_object'],
                         view.get_related_object.return_value)
        self.assertEqual(ctx['foo_object'],
                         view.get_related_object.return_value)

    def test_form_valid(self):
        self.view.get_related_field = Mock()
        self.view.get_related_field.return_value = 'foo'
        self.view.get_related_object = Mock()
        self.view.get_related_object.return_value = True
        self.view.get_success_url = Mock()
        self.view.get_success_url.return_value = '/foo'

        mock_form = Mock()
        mock_form.cleaned_data = dict(fam='dam')

        mock_object = Mock()
        mock_object.save = Mock()

        self.view.model = Mock()
        self.view.model.return_value = mock_object

        self.view.form_valid(mock_form)

        self.view.model.assert_called_once_with(fam='dam')
        self.view.object = mock_object

        mock_object.save.assert_called_once()
        self.view.object.foo = self.view.get_related_object.return_value

